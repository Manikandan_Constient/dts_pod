var express = require('express');
var router = express.Router();
var db = require('./../config/db.js');
router.post('/login', function(req, res) {
    db.bidUserLogin(req.body.useremail, req.body.pwd, function(str) {
        res.send(str);
    });

});

router.post('/forgotaccesscode', function(req, res) {
    db.userforgotPwd(req.body.useremail, function(str) {
        res.send(str);
    });

});

module.exports = router;