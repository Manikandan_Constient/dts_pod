var express = require('express');
var router = express.Router();
var db = require('./../config/db.js');
router.post('/login', function(req, res) {
    db.userLogin(req.body.useremail, req.body.pwd, function(str) {
        res.send(str);
    });

});

router.get('/getUser', function(req, res) {
    db.getUser(req.query.email, function(str) {
        res.send(str);
    });

});
module.exports = router;