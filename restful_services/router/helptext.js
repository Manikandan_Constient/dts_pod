var express = require('express');
var router = express.Router();
var db = require('./../config/db.js');

router.get('/allpages', function(req, res) {
    db.getAllPages(req, function(str) {
        res.send(str);
    });
});

router.get('/', function(req, res) {
    db.getPageHelpText(req, function(str) {
        res.send(str);
    });
});

module.exports = router;
