// var express = require('express');
// var router = express.Router();
// var db = require('./../config/db.js');
// var json2csv = require('json2csv');
// var fs = require('fs')


// router.get('/', function(req, res) {
//     db.downloadReviewBids(function(str) {

//         var fields = ["laneId", "fromCity", "fromState", "originPostalCode", "toCity", "toState", "destPostalCode", "", "distanceMiles", "refrigerated", "dropTrailors", "WayLH", "", "RTLH", "", "carrierName"];
//         var fieldNames = ["LaneID", "Origin City", "Origin State", "Origin Postal Code", "Dest City", "Dest State", "Dest Postal Code", "Annual Line Haul", "One Way Mileage", "Refrigerated?", "Drop Trailers Required", "OTR Rate", "Intermodal Rate", "Round Trip Rate", "Pepperidge Additional Information", "Carrier Name"];
//         str = JSON.parse(str)
//         var csv = json2csv({ data: str, fields: fields, fieldNames: fieldNames });

//         fs.writeFile('file.csv', csv, function(err) {
//             if (err) throw err;
//             res.setHeader('Content-disposition', 'attachment; filename=Translytix-ReviewBids.csv');
//             res.set('Content-Type', 'text/csv');
//             res.status(200).send(csv);
//         });
//     });
// });

// module.exports = router;
var express = require('express');
var router = express.Router();
var Excel = require("exceljs");
var tempfile = require('tempfile');
var db = require('./../config/db.js');
var dateFormat = require('dateformat');
var moment = require('moment-timezone');

router.get('/', function(req, res) {
    try {
        var workbook = new Excel.Workbook();
        var worksheet = workbook.addWorksheet('Sheet1');

        worksheet.columns = [
            { header: 'LaneID', key: 'id', width: 12 },
            { header: 'Carrier Name', key: 'carriername', width: 12 },
            { header: 'Origin City', key: 'OriginCity', width: 32 },
            { header: 'Origin State', key: 'OriginState', width: 12 },
            { header: 'Origin Postal Code', key: 'OriginPostalCode', width: 12 },
            { header: 'Dest City', key: 'DestCity', width: 32 },
            { header: 'Dest State', key: 'DestState', width: 12 },
            { header: 'Dest Postal Code', key: 'DestPostalCode', width: 12 },
            { header: 'Annual Line Haul', key: 'AnnualLineHaul', width: 12 },
            { header: 'One Way Mileage', key: 'OneWayMileage', width: 12 },
            { header: 'OTR Rate', key: 'OTRRate', width: 12 },
            { header: 'Intermodal Rate', key: 'IntermodalRate', width: 12 },
            { header: 'Round Trip Rate', key: 'RoundTripRate', width: 12 },
            { header: 'Team Rate', key: 'TeamRate', width: 12 },
            { header: 'Carrier Additional Info', key: 'CarrierAdditionalInfo', width: 40 },
            { header: 'BID Date Time', key: 'BIDDateTime', width: 12 },
            { header: 'OTR Rate Required', key: 'OTRRateRequired', width: 12 },
            { header: 'Intermodal Rate Required', key: 'IntermodalRateRequired', width: 12 },
            { header: 'Round Trip Rate Required', key: 'RoundTripRateRequired', width: 12 },
            { header: 'Team Rate Required', key: 'TeamRateRequired', width: 12 },
            { header: 'Refrigerted Required', key: 'RefrigertedRequired', width: 12 },
            { header: 'Shipper Additional Info', key: 'ShipperAdditionalInfo', width: 40 },
        ];

        db.downloadReviewBids(function(str) {
            console.log(str.length)
            str = JSON.parse(str)
            console.log(str.length)
            str.forEach(function(value, key) {
                worksheet.addRow({
                    id: value.laneId,
                    carriername: value.carrierName,
                    OriginCity: value.fromCity,
                    OriginState: value.fromState,
                    OriginPostalCode: value.originPostalCode,
                    DestCity: value.toCity,
                    DestState: value.toState,
                    DestPostalCode: value.destPostalCode,
                    AnnualLineHaul: value.annualLineHaul,
                    OneWayMileage: value.distanceMiles,
                    OTRRate: value.OTRRate,
                    IntermodalRate: value.intermodalRate,
                    RoundTripRate: value.roundTripRate,
                    TeamRate: value.teamRate,
                    CarrierAdditionalInfo: value.carrierAdditionalInfo,
                    BIDDateTime: convertTimezone(req.query.timezone, value.bidDate),
                    OTRRateRequired: value.OTRRateReq == 0 ? 'No' : 'Yes',
                    IntermodalRateRequired: value.intermodalRateReq == 0 ? 'No' : 'Yes',
                    RoundTripRateRequired: value.roundTripRateReq == 0 ? 'No' : 'Yes',
                    TeamRateRequired: value.teamRateReq == 0 ? 'No' : 'Yes',
                    RefrigertedRequired: value.refrigeratedReq == 0 ? 'No' : 'Yes',
                    ShipperAdditionalInfo: ((value.shipperAdditionalInfo == 'NULL') || (value.shipperAdditionalInfo == 'undefined')) ? '' : value.shipperAdditionalInfo
                });

                if (str.length == key + 1) {
                    worksheet.getRow(1).font = { name: 'Calibri', size: 11, bold: true, bgcolor: { argb: 'FF00FF00' }, }
                    var tempFilePath = tempfile('.xls');
                    workbook.xlsx.writeFile(tempFilePath).then(function() {
                        console.log('file is written');
                        res.setHeader('Content-Type', 'vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        res.setHeader('Content-disposition', 'attachment; filename=Reviewbids.xlsx');
                        res.sendFile(tempFilePath, function(err) {
                            console.log('---------- error downloading file: ' + err);
                        });
                    });
                }
            })

        })
    } catch (err) {
        console.log('OOOOOOO this is the error: ' + err);
    }
})

function convertTimezone(timezone, dbtime) {
    var server_moment = moment(dbtime);
    server_moment.format('MM/DD/YYYY h:mm A');
    var client_moment = server_moment.clone();
    client_moment.tz(timezone);
    return client_moment.format('MM/DD/YYYY h:mm A')
}
module.exports = router;