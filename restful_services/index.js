/* packages*/

var unzip = require('unzip');
var db = require('./config/db.js');
var format = require('date-format');
var credential = require('./config/credentials.js');
var express = require('express');
var cors = require('cors')
var app = express();
var json2csv = require('json2csv');
var fs = require('fs');
app.use(cors())
var bodyParser = require('body-parser')
app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
    extended: true
}));

var users = require('./router/users.js');
app.use('/users', users);
var depots = require('./router/depots.js');
app.use('/depots', depots);
var shipments = require('./router/shipments.js');
app.use('/shipments', shipments);
var settings = require('./router/settings.js');
app.use('/settings', settings);
var shipmentdetails = require('./router/shipmentdetails.js');
app.use('/shipmentdetails', shipmentdetails);
var facilityshipments = require('./router/facilityshipments.js');
app.use('/facilityshipments', facilityshipments);
var bidusers = require('./router/bidusers.js');
app.use('/bidusers', bidusers);
var mybids = require('./router/mybids.js');
app.use('/mybids', mybids);
var helptext = require('./router/helptext.js');
app.use('/helptext', helptext);
var searchReviewBids = require('./router/searchReviewBids.js');
app.use('/searchReviewBids', searchReviewBids);
var searchLaneOpportunities = require('./router/searchLaneOpportunities.js');
app.use('/searchLaneOpportunities', searchLaneOpportunities);
var downloadreviewbids = require('./router/downloadreviewbids.js');
app.use('/downloadreviewbids', downloadreviewbids);
var chkcarrier = require('./router/chkcarrier.js');
app.use('/chkcarrier', chkcarrier);
var gl = require('./router/gl.js');
app.use('/gl', gl);
var forgotaccess = require('./router/forgotaccess.js');
app.use('/forgotaccess', forgotaccess);
var getVerbiage = require('./router/getVerbiage.js');
app.use('/getVerbiage', getVerbiage);

var fetchCarrierUsers = require('./router/fetchCarrierUsers.js');
app.use('/fetchCarrierUsers', fetchCarrierUsers);

var uploadlane = require('./router/uploadlane.js');
app.use('/uploadlane', uploadlane);

var email = require('./email/email.js');
app.use('/email', email);

app.listen(8080);