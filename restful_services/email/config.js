﻿"use strict";

var config = {
    "templateBucket": "poduat-configurations",
    "templateKey": "email/bid_body_template.html",
    "bidTemplateBucket": "poduat-configurations",
    "bidUserRegnTemplateKey": "email/REQUEST_BID_ACCESS.tpl.html",
    "generalTemplateKey": "email/GENERAL_TEMPLATE.tpl.html",
    "forgotAccessStatusPendingTemplateKey": "email/FORGOT_ACCESS_STATUS_PENDING.html",
    "bidAdminRegnTemplateKey": "email/REQUEST_BID_ACCESS_ADMIN.tpl.html",
    "bidUserAcceptTemplateKey": "email/ACCEPT_BID_ACCESS_ADMIN.tpl.html",
    "bidTemplateKey": "email/make_bid_email.tpl.html",
    "targetAddress": "manikandan@constient.com",
    "fromAddress": "<support@translytix.com>",
    "defaultSubject": "Email From BID"
}

module.exports = config