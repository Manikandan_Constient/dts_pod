/* packages*/

var AWS = require('aws-sdk');
// var s3 = new aws.S3({ apiVersion: '2006-03-01' });
var unzip = require('unzip');
var stream = require('stream');
var gm = require('gm').subClass({ imageMagick: true })
var util = require("util");
var fs = require('fs');
var mktemp = require("mktemp");
var unzipToS3 = require('unzip-to-s3');
var async = require('async');
var readline = require('readline');
var jsonfile = require('jsonfile');
var mysql = require('mysql');
AWS.config.update({ region: 'us-west-2' });
var format = require('date-format');


// constants
var MAX_WIDTH = 100,
    MAX_HEIGHT = 100;


// get reference to S3 client
var s3 = new AWS.S3();
//SES
var ses = new AWS.SES();
var config = require('./config.js');
/******************* INSTANCE VARIABLES ******************/

//MYSQL CONNECTION - Production
// var connection = mysql.createConnection({
//     host: '35.162.123.99',
//     user: 'poduser',
//     password: 'Passw0rd1',
//     database: 'dts_pod'
// });


// connection.connect()
// var params = {
//     SOURCE_BUCKET: 'pod-mobile',
//     DEST_BUCKET: 'pod-prod'
// };
//-------------------------------------------------------------------------------------------------------------------

//MYSQL CONNECTION - UAT

var connection = mysql.createConnection({
    host: '35.167.222.82',
    user: 'poduser',
    password: 'Passw0rd1',
    database: 'pod_uat'
});

connection.connect()
var params = {
    SOURCE_BUCKET: 'poduat-mobile',
    DEST_BUCKET: 'poduat-prod'
};

//---------------------------------------------------------------------------------------------------------------------
var folderToCreate;

var obj;
var flepath
var fname
var ftype
var fsize
var fthumbnailurl
var furl
    /******************* INSTANCE VARIABLES ******************/
var callbackglobal = function(a, b) {

};

exports.handler = function(event, context, callback) {
    var zipfile = event.Records[0].s3.object.key;
    // var zipfile = '20170324151634-83838444.zip';
    //start processing
    // callbackglobal = callback;
    showlog('calling process...' + zipfile);
    process(zipfile);
}

var uploadDataKey
var folderKey;
//unzip and upload into destination bucket
function process(zipfile) {
    showlog('inside process...' + zipfile);
    var cnt = 0;
    s3.getObject({ Bucket: params.SOURCE_BUCKET, Key: zipfile },
        function(err, data) {
            if (err) {
                showlog('error: ' + err);
            } else {

                //Take count 
                var bufferStream1 = new stream.PassThrough();
                bufferStream1.end(new Buffer(data.Body));
                var unzipParser1 = unzip.Parse();
                var total_files = 0;
                bufferStream1.pipe(unzipParser1).on('entry', function(entry) {
                    var filename = entry.path;
                    showlog('filename====' + filename);
                    total_files++;
                    entry.autodrain();
                });
                unzipParser1.on('close', function() {
                    showlog('total_files=' + total_files);
                });

                //Actual processing
                var bufferStream = new stream.PassThrough();
                bufferStream.end(new Buffer(data.Body));
                var unzipParser = unzip.Parse();
                var check_cnt = 1;
                bufferStream.pipe(unzipParser).on('entry', function(entry) {
                    var filename = entry.path;
                    folderToCreate = zipfile.split('.')[0];
                    folderToCreate = folderToCreate + "/" + filename;
                    s3.upload({ Bucket: params.DEST_BUCKET, Key: folderToCreate, Body: entry, ACL: 'public-read' },
                        function(err, data) {
                            showlog('check_cnt====' + check_cnt);
                            showlog('data.key====' + data.key);
                            var file_ext = (data.key).split('.')[1];
                            console.log("Trying for Thumbnail Upload: " + file_ext)
                            if (file_ext == 'jpg') {
                                uploadDataKey = folderToCreate
                            }
                            if ((file_ext == 'json') || (file_ext == 'JSON')) {
                                folderKey = data.key

                            }

                            //Read json message file while uploading                                         
                            if (check_cnt == total_files) {
                                // fileread(folderKey);
                                fileread(folderKey);
                            }
                            check_cnt++;
                        });
                    entry.autodrain();
                });

            }
        });
}

//Code to create and upload thumbnail_url

function uploadThumbnail(folderToCreate) {
    var typeMatch = folderToCreate.match(/\.([^.]*)$/);
    var imageType = typeMatch[1];
    async.waterfall([
        function download(next) {
            s3.getObject({ Bucket: params.DEST_BUCKET, Key: folderToCreate }, next)
        },
        function transform(response, next) {
            console.log("Upload Thumbnail--->>> " + JSON.stringify(response))
            gm(response.Body).size(function(err, size) {
                var scalingFactor = Math.min(
                    MAX_WIDTH / size.width,
                    MAX_HEIGHT / size.height
                );
                var width = scalingFactor * size.width;
                var height = scalingFactor * size.height;
                console.log("Width..." + width + " // Height..." + height)
                    // Transform the image buffer in memory.
                this.resize(width, height)
                    .toBuffer(imageType, function(err, buffer) {
                        if (err) {
                            console.log("Error while transform..." + err)
                            next(err);
                        } else {
                            next(null, response.contentType, buffer)
                        }
                    });
            })
        },
        function upload(contentType, data, next) {
            // Stream the transformed image to a different S3 bucket.
            console.log("Upload Data is:" + JSON.stringify(data))
            s3.putObject({
                Bucket: params.DEST_BUCKET,
                ACL: 'public-read',
                Key: folderToCreate + "_thumbnail" +
                    ".png",
                Body: data,
                ContentType: 'PNG'
            });
            //deleteObjects(oldPrefixVal);
        }
    ])
}

//Code to read MESSAGE Json file
function fileread(folderToCreate) {
    showlog('inside fileread...' + folderToCreate);
    s3.getObject({ Bucket: params.DEST_BUCKET, Key: folderToCreate },
        function(err, data) {
            if (data != null) {
                var resultData = data.Body.toString();
                var json = JSON.stringify(eval('(' + resultData + ')'));
                obj = JSON.parse(json);
                showlog("OBJECT:::::::::::::::" + JSON.stringify(obj));
                var tempFolderName = obj.shipment + '#' + obj.delivery + '#' + obj.shipto;
                var oldFoldName = folderToCreate.split("/")[0]
                    //Rename with newly created folder with formatted name
                showlog('oldFoldName=' + oldFoldName + ",tempFolderName=" + tempFolderName);
                folderRen(oldFoldName, tempFolderName, folderToCreate)
            }
        })
}


//Rename folder in S3 => Copy and Paste files into new folder, delete old folder
var shipmentDetails = []

function cb(err) {
    showlog('cb 1=');
    showlog('err=' + err);
}

function cb() {
    showlog('cb 2=');
}

var done = function(err, data) {
    if (err) showlog(err);
    else showlog(data);
};

var oldPrefixVal;

function folderRen(oldPrefix, newPrefix, folderToCreate) {
    showlog('inside folderRen...oldPrefix=' + oldPrefix + ",newPrefix=" + newPrefix);
    oldPrefixVal = oldPrefix
    var paramList = {
        Bucket: params.DEST_BUCKET,
        Delimiter: '/',
        Prefix: oldPrefix + "/"
    };
    s3.listObjects(paramList, function(err, data) {
        var count = 0;
        if (data.Contents.length) {
            async.each(data.Contents, function(file, cb) {
                count++;
                flepath = file.Key.split('/');
                var param = {
                    Bucket: params.DEST_BUCKET,
                    MetadataDirective: 'REPLACE',
                    CopySource: params.DEST_BUCKET + "/" + file.Key,
                    Key: newPrefix + "/" + flepath[1],
                    ACL: 'public-read'
                };
                showlog('file=' + JSON.stringify(file));
                showlog('file.Key=' + file.Key);
                showlog('flepath[0]=' + flepath[0]);
                showlog('flepath[1]=' + flepath[1]);
                showlog('param.Key=' + param.Key);
                fname = flepath[1].split('.')[0]
                ftype = flepath[1].split('.')[1]
                fsize = file.Size
                fthumbnailurl = file.Key.replace(oldPrefix, newPrefix)
                furl = file.Key.replace(oldPrefix, newPrefix)
                shipmentDetails.push({
                    'fileName': fname,
                    'fileType': ftype,
                    'fileSize': fsize,
                    'thumbnailUrl': fthumbnailurl,
                    'fileUrl': furl
                })
                s3.copyObject(param, function(copyErr, copyData) {
                    if (copyErr) {
                        showlog("COPY ERROR:: :: ::" + copyErr);
                    } else {
                        cb();
                        // showlog("iside cb");
                    }
                });
                showlog(count)
                if (count == data.Contents.length) {
                    uploadThumbnail(uploadDataKey)
                        // deleteObjects(oldPrefix);
                    showlog("shipmentDetailsKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK" + JSON.stringify(shipmentDetails))
                        // insert_shipments(shipmentDetails);
                }
            }, done);
        }
    });
}

//delete 
function deleteObjects(oldPrefix) {
    showlog('inside deleteObjects...' + oldPrefix);
    var paramList = {
        Bucket: params.DEST_BUCKET,
        Delimiter: '/',
        Prefix: oldPrefix + "/"
    };

    s3.listObjects(paramList, function(err, data) {
            var count = 0;
            if (data.Contents.length) {
                async.each(data.Contents, function(file, cb) {
                    count++;
                    var flepath = file.Key.split('/');
                    var deleteParam = {
                            Bucket: params.DEST_BUCKET,
                            Key: oldPrefix + "/" + flepath[1]
                        }
                        /* fname = flepath[1].split('.')[0]
                         ftype = flepath[1].split('.')[1]
                         fsize = file.Size
                         fthumbnailurl = file.Key
                         furl = file.Key
                         shipmentDetails = ''
                         shipmentDetails = {
                             'fileName': fname,
                             'fileType': ftype,
                             'fileSize': fsize,
                             'thumbnailUrl': fthumbnailurl,
                             'fileUrl': furl,
                         }*/
                    s3.deleteObject(deleteParam, function(copyErr, copyData) {
                        showlog("CDDDDDDDDDDDDDDDDDDDDDDDDDDDDD " + JSON.stringify(copyData))
                        if (copyErr) {
                            showlog("DELETE ERROR:: :: ::" + copyErr);
                        } else {
                            showlog('DELETED: ', deleteParam.Key);
                            // insert_shipments();
                            cb();
                        }
                    });
                    if (count == data.Contents.length) {
                        showlog("shipmentDetailsKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK" + JSON.stringify(shipmentDetails))
                        insert_shipments(shipmentDetails);
                    }
                })
            }
        })
        //INSERT INTO SHIPMENT TABLE
        //sendEmail(obj);
}

var lastinsertid;

function insert_shipments(param) {
    try {
        //connecting mysql
        console.log("MY DEPOT ID::::::::::::::::::::::" + jsontostring(obj))
        showlog("SHIPMENT DETAILS STR::::::::" + jsontostring(param))
            /*connection.connect();*/
        obj.upload_mode = 'MOBILE';
        obj.upload_dt = format('yyyy-MM-dd hh:mm:ss', new Date());
        showlog(obj.upload_dt)

        var checkDepotAvailablity = "SELECT * FROM depot WHERE depot_code ='" + obj.depot_id + "'"
        showlog('checkDepotAvailablity Query: ' + checkDepotAvailablity)
        connection.query(checkDepotAvailablity, function(err, result) {
            //showlog("DEPOT AVAILABILITY STATUS:" + JSON.stringify(result[0]))
            if (err) {
                showlog("ERROR IN checkDepotAvailablity")
                var customerData = ""
                var emailTo = []
                var emailFrom
                showlog("MY ERROR IS..........................................................:::" + err)
                var emailToQryUser = 'select * from users where role="PODADMIN"'
                showlog("emailToQryUser::::" + emailToQryUser)
                connection.query(emailToQryUser, function(err, result) {
                    showlog("BEFORE EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE" + result)
                    showlog("BEFORE EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE" + JSON.stringify(result))
                    for (var i = 0; i < result.length; i++) {
                        var x = result[i].email
                        var atpos = x.indexOf("@");
                        var dotpos = x.lastIndexOf(".");
                        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                            showlog("Found an Invalid Email: " + result[i].email)
                        } else {
                            emailTo.push(result[i].name + '<' + result[i].email + '>')
                        }
                    }
                    emailFrom = 'select svalue from settings where skey="FROM_EMAIL"'
                    connection.query(emailFrom, function(err, result) {
                        if (err) throw err;
                        emailFrom = result[0].svalue;
                        sendEmail(obj, param, customerData, emailTo, emailFrom, 'yes');
                    });
                })
            } else if (result.length > 0) {
                var query_shipment = 'Insert Into shipment (depot_id, ship_num, delivery, ship_to_customer, upload_mode, imei, upload_dt)' + ' values ("' + obj.depot_id + '", "' + obj.shipment + '","' + obj.delivery + '","' + obj.shipto + '","' + obj.upload_mode + '","' + obj.imei + '","' + (obj.upload_dt) + '")';
                showlog('query_shipment=' + query_shipment);
                connection.query(query_shipment, function(err, result) {
                    lastinsertid = result.insertId;
                    for (var i = 0; i < param.length; i++) {
                        var queryname = 'Insert Into shipment_detail (shipment_id, filename, file_type, filesize, thumbnail_url,file_url)' + ' values (' + lastinsertid + ', "' + param[i].fileName + '","' + param[i].fileType + '","' + param[i].fileSize + '","' + param[i].thumbnailUrl + '","' + param[i].fileUrl + '")';
                        connection.query(queryname, function(err, result) {
                            if (err) throw err;
                        });
                        if (i == param.length - 1) {
                            var customerData = 'SELECT * FROM depot WHERE depot_code=' + obj.depot_id
                            var emailToQry
                            var emailToQryUser
                            var emailFrom
                            var emailTo = []
                            connection.query(customerData, function(err, result) {
                                if (err) throw err;
                                customerData = result;
                                emailToQry = 'select user_email, user_name from view_depot_user where depot_code =' + obj.depot_id
                                showlog("emailTOQry::::" + emailToQry)
                                connection.query(emailToQry, function(err, result) {
                                    if (err) throw err;
                                    emailToQry = result;
                                    for (var i = 0; i < result.length; i++) {
                                        var x = result[i].user_email
                                        var atpos = x.indexOf("@");
                                        var dotpos = x.lastIndexOf(".");
                                        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                                            showlog("Found an Invalid Email: " + result[i].user_email)
                                        } else {
                                            emailTo.push(result[i].user_name + '<' + result[i].user_email + '>')
                                        }

                                    }
                                    emailFrom = 'select svalue from settings where skey="FROM_EMAIL"'
                                    connection.query(emailFrom, function(err, result) {
                                        if (err) throw err;
                                        emailFrom = result[0].svalue;
                                        sendEmail(obj, param, customerData, emailTo, emailFrom, 'no');
                                    });

                                });
                            });
                        }
                    }
                })
            } else {
                var customerData = ""
                var emailTo = []
                var emailFrom
                showlog("MY ERROR IS..........................................................:::" + err)
                var emailToQryUser = 'select * from users where role="PODADMIN"'
                showlog("emailToQryUser::::" + emailToQryUser)
                connection.query(emailToQryUser, function(err, result) {
                    showlog("BEFORE EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE" + result)
                    showlog("BEFORE EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE" + JSON.stringify(result))
                    for (var i = 0; i < result.length; i++) {
                        var x = result[i].email
                        var atpos = x.indexOf("@");
                        var dotpos = x.lastIndexOf(".");
                        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                            showlog("Found an Invalid Email: " + result[i].email)
                        } else {
                            emailTo.push(result[i].name + '<' + result[i].email + '>')
                        }
                    }
                    emailFrom = 'select svalue from settings where skey="FROM_EMAIL"'
                    connection.query(emailFrom, function(err, result) {
                        if (err) throw err;
                        emailFrom = result[0].svalue;
                        sendEmail(obj, param, customerData, emailTo, emailFrom, 'yes');
                    });
                })
            }
        })
    } catch (err) {
        showlog("Catch Error: " + err)
    }
}

function sendEmail(obj, param, customerData, emailTo, emailFrom, toAdmin) {
    showlog('Loading template from ' + config.templateKey + ' in ' + config.templateBucket);
    showlog("OBJECT ::: ::: " + JSON.stringify(obj))
    showlog("Param ::: ::: " + JSON.stringify(param))
    showlog("Customer Data ::: ::: " + JSON.stringify(customerData))
    showlog("Email To ::: ::: " + JSON.stringify(emailTo))
    showlog("Email From ::: ::: " + JSON.stringify(emailFrom))
    showlog("To Admin ::: ::: " + toAdmin)
        // Read the template file
    s3.getObject({
        Bucket: config.templateBucket,
        Key: config.templateKey
    }, function(err, data) {
        if (err) {
            // Error
            showlog(err, err.stack);
            showlog('Internal Error: Failed to load template from s3.')
        } else {
            var templateBody = data.Body.toString();
            showlog("Template Body: " + templateBody);

            var event = {};
            event.depot_id = ((customerData != "") && ((customerData[0].id) != null)) ? customerData[0].id : obj.depot_id;
            event.depot = ((customerData != "") && ((customerData[0].name) != null)) ? customerData[0].name : "";
            event.Shipment = obj.shipment;
            event.Delivery = obj.delivery;
            event.Shipto = obj.shipto;
            showlog("CHECK FOR NULL:::" + (customerData[0].address_line_1))
            event.address1 = ((customerData != "") && ((customerData[0].address_line_1) != null)) ? customerData[0].address_line_1 : "";
            event.address2 = ((customerData != "") && ((customerData[0].address_line_2) != null)) ? customerData[0].address_line_2 : "";
            event.city = ((customerData != "") && ((customerData[0].city) != null)) ? customerData[0].city : "";
            event.country = ((customerData != "") && ((customerData[0].country) != null)) ? customerData[0].country : "";
            event.zipcode = ((customerData != "") && ((customerData[0].zipcode) != null)) ? customerData[0].zipcode : "";
            event.TranslytixPOD = "Translytics POD";
            event.instoredate = obj.upload_dt;
            event.pod = "POD";
            event.ship_num = obj.shipment;
            event.delivery = obj.delivery;
            event.shiptocust = obj.shipment;
            event.upload_mode = obj.upload_mode;
            event.tableBody = ''
            if (toAdmin == 'no') {
                var msgValues = param;
                for (i = 0; i < msgValues.length; i++) {
                    if (i % 2 == 0) {
                        var uri = msgValues[i].fileUrl;
                        if (msgValues[i].fileType != 'json') {
                            uri = uri.replace("#", "%23")
                            uri = uri.replace("#", "%23")
                            uri = "https://poduat-prod.s3.amazonaws.com/" + uri
                            event.tableBody = event.tableBody + '<tr style="border-bottom: 1px solid rgba(0,0,0,0.30); border-collapse: collapse; background: #FAFAFA; color: rgba(0, 0, 0, 0.87)">' + '<td  style="text-align: center !important; border-bottom: 1px solid rgba(0,0,0,0.30); border-collapse: collapse;padding:10px">' + msgValues[i].fileName + '</td>' + '<td  style="text-align: center !important; border-bottom: 1px solid rgba(0,0,0,0.30); border-collapse: collapse;padding:10px">' + msgValues[i].fileType + '</td>' + '<td  style="text-align: center !important; border-bottom: 1px solid rgba(0,0,0,0.30); border-collapse: collapse;padding:10px">' + msgValues[i].fileSize + '</td>' + '<td  style="text-align: center !important; border-bottom: 1px solid rgba(0,0,0,0.30); border-collapse: collapse;padding:10px">' + msgValues[i].thumbnailUrl + '</td>' + '<td  style="text-align: center !important; border-bottom: 1px solid rgba(0,0,0,0.30); border-collapse: collapse;padding:10px">' + uri + '</td>' + '</tr>'
                        }
                    } else {
                        if (msgValues[i].fileType != 'json') {
                            event.tableBody = event.tableBody + '<tr style="border-bottom: 1px solid rgba(0,0,0,0.30); border-collapse: collapse; background: #F5F5F5; color: rgba(0, 0, 0, 0.87)">' + '<td  style="text-align: center !important; border-bottom: 1px solid rgba(0,0,0,0.30); border-collapse: collapse;padding:10px">' + msgValues[i].fileName + '</td>' + '<td  style="text-align: center !important; border-bottom: 1px solid rgba(0,0,0,0.30); border-collapse: collapse;padding:10px">' + msgValues[i].fileType + '</td>' + '<td  style="text-align: center !important; border-bottom: 1px solid rgba(0,0,0,0.30); border-collapse: collapse;padding:10px">' + msgValues[i].fileSize + '</td>' + '<td  style="text-align: center !important; border-bottom: 1px solid rgba(0,0,0,0.30); border-collapse: collapse;padding:10px">' + msgValues[i].thumbnailUrl + '</td>' + '<td  style="text-align: center !important; border-bottom: 1px solid rgba(0,0,0,0.30); border-collapse: collapse;padding:10px">' + uri + '</td>' + '</tr>'
                        }
                    }
                }
            } else {
                var msgValues = "Please create a depot id for " + event.depot_id
                event.tableBody = '<tr style="border-collapse: collapse; background: #FAFAFA; color: rgba(0, 0, 0, 0.87)">' + '<td  style="text-align: center !important; border-collapse: collapse;padding:10px">' + msgValues + '</td></tr>'
            }
            showlog(event.tableBody)

            // Perform the substitutions
            var mark = require('markup-js');
            //Email Content Preparations
            var message = mark.up(templateBody, event);
            showlog("Final message: " + message);
            if (true) {
                if (toAdmin == 'no') {
                    var params = {
                        Destination: {
                            ToAddresses: emailTo
                        },
                        Message: {

                            Subject: {
                                Data: event.depot_id + " - " + event.instoredate + " - " + event.pod,
                                Charset: 'UTF-8'
                            }
                        },
                        //Source: config.fromAddress
                        Source: emailFrom
                            // ,
                            // ReplyToAddresses: [
                            //     event.name + '<' + 'mail2niazahamed@gmail.com' + '>'
                            // ]
                    };
                } else {
                    var params = {
                        Destination: {
                            ToAddresses: emailTo
                        },
                        Message: {

                            Subject: {
                                Data: "Req to create depot for - " + event.depot_id,
                                Charset: 'UTF-8'
                            }
                        },
                        //Source: config.fromAddress
                        Source: emailFrom
                            // ,
                            // ReplyToAddresses: [
                            //     event.name + '<' + 'mail2niazahamed@gmail.com' + '>'
                            // ]
                    };
                }
                showlog("Sending Mail to ::: " + emailTo)
                var fileExtension = config.templateKey.split(".").pop();
                if (fileExtension.toLowerCase() == 'html') {
                    params.Message.Body = {
                        Html: {
                            Data: message,
                            Charset: 'UTF-8'
                        }
                    };
                } else if (fileExtension.toLowerCase() == 'txt') {
                    params.Message.Body = {
                        Text: {
                            Data: message,
                            Charset: 'UTF-8'
                        }
                    };
                } else {
                    showlog('Internal Error: Unrecognized template file extension: ' + fileExtension);
                    return;
                }

                showlog(params);
                // Send the email
                ses.sendEmail(params, function(err, data) {
                    if (err) {
                        showlog(err, err.stack);
                        showlog('Internal Error: The email could not be sent.');

                    } else {
                        showlog(data); // successful response
                        showlog('Email was successfully sent !');
                        // callbackglobal(null, "success");
                        // connection.end();
                        // return;
                    }
                    callbackglobal(null, "success");
                    connection.end();
                    return;
                });
            }
        }
    });
}

function showlog(str) {
    console.log(str);
}


function jsontostring(str) {
    return JSON.stringify(str);
}